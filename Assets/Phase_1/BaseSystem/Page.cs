﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Page : ManagableObject
{

    
    [SerializeField] private State _state;
    [SerializeField] private List<UIElement> _elements;

    protected bool Awaken { get; private set; } = false;

    protected virtual void InternalAwake()
    {
        Connect(this, _state);
        foreach (var element in _elements)
        {
            element.Init(this);
        }
    }
    protected void Awake()
    {
        if (!_state) return;
        InternalAwake();
        Awaken = true;
    }

    protected bool SetState(State state)
    {
        if (Awaken) return false;
        _state = state;
        return true;
    }

    

    public void TriggerDownsideEvent(int actionId, ActionData data)
    {
        TriggerDownside(actionId, data);
    }
    public event Action<int, ActionData> UpsideEvent
    {
        add { UpSideAction += value; }
        remove { UpSideAction -= value; }
    }
}