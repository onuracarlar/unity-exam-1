﻿using UnityEngine;

public class PageInstancer : MonoBehaviour
{
    [SerializeField] private State _state;
    [SerializeField] private PageInstancable _pageInstancable;

    void Awake()
    {
        if (_state && _pageInstancable)
            Instantiate(_pageInstancable.gameObject, transform).GetComponent<PageInstancable>().Attach(_state);
    }
}