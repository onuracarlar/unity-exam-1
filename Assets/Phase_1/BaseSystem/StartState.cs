﻿using System.Collections;

public class StartState : State
{
    IEnumerator Start()
    {
        for (int i = 0; i < 5; i++)
        {
            yield return null;
        }
        TriggerEnter(null);
    }
}