﻿using System;
using UnityEngine;

public abstract class UIElement : MonoBehaviour
{

    [NonSerialized] private Page _page;
    [SerializeField] private int _enterActionId = 100;
    [SerializeField] private int _exitActionId = 200;
    public void Init(Page page)
    {
        Hide(null);
        _page = page;
        _page.UpsideEvent += _page_UpsideEvent;
    }

    private void _page_UpsideEvent(int arg1, ActionData arg2)
    {
        if (arg1 == _enterActionId) Show(arg2);
        else if (arg1 == _exitActionId) Hide(arg2);
        else ActionTriggered?.Invoke(arg1, arg2);
    }

    protected virtual void Start() {  }
    protected abstract void Show(ActionData data);
    protected abstract void Hide(ActionData data);
    protected void TriggerAction(int actionId, ActionData data) => _page.TriggerDownsideEvent(actionId, data);
    protected event Action<int, ActionData> ActionTriggered;

}