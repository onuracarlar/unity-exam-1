﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonElement : UIElement
{
    [SerializeField] private Image _icon;
    [SerializeField] private Button _button;
    [SerializeField] private Text _buttonText;
    [SerializeField] private int _actionId = -1;

    protected override void Start()
    {
        if (_button)
        {
            var clickEvent = _button.onClick;
            clickEvent.AddListener(Trigger);
            _button.onClick = clickEvent;
        }
        base.Start();
    }

    protected virtual void Trigger()
    {
        TriggerAction(_actionId, null);
    }

    protected override void Show(ActionData data)
    {
        if (_icon) _icon.enabled = true;
        if (_button) _button.enabled = true;
        if (_buttonText) _buttonText.enabled = true;
    }

    protected override void Hide(ActionData data)
    {
        if (_icon) _icon.enabled = false;
        if (_button) _button.enabled = false;
        if (_buttonText) _buttonText.enabled = false;
    }
}