﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Content Database", menuName = "Content Database")]
public class ContentDatabase : ScriptableObject
{
    [SerializeField] private List<TextContent> _textContents;
    [SerializeField] private List<ImageContent> _imageContents;

    // edit

    [SerializeField] private Texture2D avatar;
    
    public void SetAvatarImage(Texture2D texture)
    {
        
         avatar = texture;
        
      
    }
    public Texture GetAvatarImage()
    {
        return avatar;
    }
    
    //edit 

    public void AddContent(int id, string content)
    {
        _textContents.Add(new TextContent(id, content));
    }

    public void AddContent(int id, Texture2D content)
    {
        _imageContents.Add(new ImageContent(id, content));
    }

    //public void SetContent(int id, string content)
    //{
    //     RemoveTextContents(id);
    //    AddContent(id, content);
    //}
    //public void SetContent(int id, Texture2D content)
    //{
    //    RemoveTextContents(id);
    //    AddContent(id, content);
    //}

    //Removes all kind of contents with the id
    public void RemoveContents(int id)
    {
        RemoveContents(id);
        RemoveImageContents(id);
    }

    public void RemoveTextContents(int id)
    {
        var textContents = GetTextContents(id);
        textContents.ForEach(x => _textContents.Remove(x));
    }

    public void RemoveImageContents(int id)
    {
        var imageContents = GetImageContents(id);
        imageContents.ForEach(x => _imageContents.Remove(x));
    }

    public List<TextContent> GetTextContents(int id)
    {
        return _textContents.FindAll(x => x.Id.Equals(id));
    }
    public List<ImageContent> GetImageContents(int id)
    {
        return _imageContents.FindAll(x => x.Id.Equals(id));
    }
    public List<ImageContent> GetAllImageContents()
    {
        return _imageContents;
    }

    public IEnumerator<TextContent> GetTextEnumerator()
    {
        return _textContents.GetEnumerator();
    }
    public IEnumerator<ImageContent> GetImageEnumerator()
    {
        return _imageContents.GetEnumerator();
    }

    [Serializable]
    public abstract class ContentBase<T>
    {
        [SerializeField] public int Id;
        [SerializeField] public T Content;

        protected void Set(int id, T content)
        {
            Id = id;
            Content = content;
        }
    }

    [Serializable]
    public class DatabaseList : ContentBase<ContentDatabase>
    {
        
    }

    [Serializable]
    public class TextContent : ContentBase<string>
    {
        public TextContent(int id, string content)
        {
            Set(id,content);
        }
    }

    [Serializable]
    public class ImageContent : ContentBase<Texture2D>
    {
        public ImageContent(int id, Texture2D content)
        {
            Set(id, content);
        }
    }
}

