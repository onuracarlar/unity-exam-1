﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventTriggerElement : UIElement
{
    public UnityEvent ShowEvent;
    public UnityEvent HideEvent;

    protected override void Hide(ActionData data)
    {
       
        HideEvent.Invoke();
    }

    protected override void Show(ActionData data)
    {
        ShowEvent.Invoke();
    }

}
