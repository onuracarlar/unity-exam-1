﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageElement : UIElement
{
    //TODO Implement
    [SerializeField] private RawImage _image;
   
   
    public ContentDatabase imagesCD;
   // int imageId;

    protected override void Hide(ActionData data)
    {
        //TODO Implement
        if (_image) _image.enabled = false;
    }

    protected override void Show(ActionData data)
    {
        //TODO Implement
        
        

        if (_image) _image.enabled = true;

        if (gameObject.tag == "Avatar")
        {
           _image.GetComponent<RawImage>().texture = imagesCD.GetAvatarImage();
        }
    }

 
    
}