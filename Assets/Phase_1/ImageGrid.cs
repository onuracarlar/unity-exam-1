﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageGrid : UIElement {

    [SerializeField] private RawImage _image;
    public ContentDatabase imagesCD;
    private List<RawImage> childrenImagesList;
    private List<Button> childrenButtonsList;

    private void Awake()
    {
        
        childrenImagesList = new List<RawImage>();
        childrenButtonsList = new List<Button>();

        for (int i = 0; i < GetComponentsInChildren<RawImage>().Length; i++)
        {
            childrenImagesList.Add(GetComponentsInChildren<RawImage>()[i]);
            childrenButtonsList.Add(GetComponentsInChildren<Button>()[i]);
        }
        Debug.Log(childrenButtonsList.ToArray().Length);
        Debug.Log(imagesCD.GetAllImageContents().ToArray().Length);
        for (int i = 0; i < imagesCD.GetAllImageContents().ToArray().Length; i++)
        {
            childrenImagesList[i].texture = imagesCD.GetAllImageContents()[i].Content;
           
           
        }
        childrenButtonsList[0].onClick.AddListener(() => imagesCD.SetAvatarImage(imagesCD.GetAllImageContents()[0].Content));
        childrenButtonsList[1].onClick.AddListener(() => imagesCD.SetAvatarImage(imagesCD.GetAllImageContents()[1].Content));
        childrenButtonsList[2].onClick.AddListener(() => imagesCD.SetAvatarImage(imagesCD.GetAllImageContents()[2].Content));
        childrenButtonsList[3].onClick.AddListener(() => imagesCD.SetAvatarImage(imagesCD.GetAllImageContents()[3].Content));
        childrenButtonsList[4].onClick.AddListener(() => imagesCD.SetAvatarImage(imagesCD.GetAllImageContents()[4].Content));
        
    }

    protected override void Show(ActionData data)
    {
        if (_image) _image.enabled = true;
    }

    protected override void Hide(ActionData data)
    {
        if (_image) _image.enabled = false;
    }

    
}
