﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextElement : UIElement {

    [SerializeField] private Text _text;
    public ContentDatabase cd;

    protected override void Show(ActionData data)
    {
        if (_text) _text.enabled = true;
        if (gameObject.tag=="NickName")
        {
            _text.text = cd.GetTextContents(11)[0].Content;
        }
    }

    protected override void Hide(ActionData data)
    {
        if (_text) _text.enabled = false;
    }
}