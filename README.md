# Unity Exam 1

There is a 2-layered software structure in this project.

The first layer has State objects. State objects can be linked with a limited set of rules.
Only one state can be active at the time.
State system handles connections. State objects are configurable via the editor.

The second layer contains UIElements,
UIElements are basically the core elements of user interaction.
They can display contents and get input data from a user then trigger actions.

Between two layers there are Page objects, who creates the link between State objects and UIElements.

UIElements gets their Hide and Show call from State objects and trigger State objects' Actions with id and ActionData via Page object. Do not directly reference UIElements if they don't share the same Page object. If you do, you will lose the references when you create prefabs from Pages.

In the 2nd Phase, you will use the pages you created in this phase as templates. And instance them on runtime via PageInstancers. The purpose of instancing is making the pages reusable in different states with different content and different action responses. 

Normally the logic of the system must be relay on State layer, but in order to simplify the system, State system kept with its basic functionality. You can create logic in UIElements via ActionData and Action Ids.


# <b><i>Rules</i></b>

Don't change any class in BaseSystem folder!
Only inherit UIElement and ContentDatabase classes!

Do not access Stage or Page class directly, use UIElement functions and events to create interactivity between screens.

If there is a button that not linked, link it.
If there is a bug in this template, fix it without compromising the structure.

Good Luck.

# Start Point

You can start with Phase_1/Phase_1.scene